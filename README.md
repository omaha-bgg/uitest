# Using the git tree

This package is using `setuptools` configured with `pyproject.toml`.

Setup current venv for development:

 pip install -r requirements-dev.txt

Run tests:

 ./test/test.py

NOTE: it is recommended to run the tests in a separate graphical to
avoid interactions with the user session, e.g. by using `xvfb-run` to
launch the tests.

Build package:

 python -m build
