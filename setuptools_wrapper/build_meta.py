"""Setuptools-based buildsystem for generating data files.

This is a PEP-517-compatible buildsystem that only overrides
build_sdist() to generate our xml data files priori to building a
package.
"""

import os
import xml.etree.ElementTree as ET
import setuptools.build_meta

def gen_xml_files(datadir, atspicore):
    for dirname, subdirs, files in os.walk(datadir):
        # for each data/*.in file ...
        assert not subdirs
        for basename in files:
            if not basename.endswith(".in"):
                continue
            infile = os.path.join(dirname, basename)
            # ... get list of at-spi2-core partial D-BUS definitions
            with open(infile, "r") as inf:
                lines = [line.strip() for line in inf.readlines()]
            # ... use them to create a full-fledged D-BUS definition
            out_elt = ET.Element("node")
            for atspicore_xml in lines:
                in_et = ET.parse(os.path.join(atspicore, atspicore_xml))
                for child in in_et.getroot():
                    out_elt.append(child)
            # ... patch it if needed
            patch(out_elt, basename)
            # ... and dump it to data/*.xml
            outfile = infile[:-3] + ".xml"
            out_et = ET.ElementTree(out_elt)
            out_et.write(outfile)

def patch(elt, basename):
    if basename == "Registry.in":
        # fix EventListenerRegistered to match Gtk 3.24.24
        signal = elt.find('./interface/signal[@name="EventListenerRegistered"]')
        assert signal
        assert len(signal) == 2
        ET.SubElement(signal, "arg", name="properties", type="as")
        assert len(signal) == 3

        # disable usage of optional RegisterEvent params
        # Those only come starting with 2.45.1, we likely don't them, and supporting
        # them is likely to be a problem if we want backward compat.
        method = elt.find('./interface/method[@name="RegisterEvent"]')
        assert method
        assert len(method) == 3
        del method[1:3]
        assert len(method) == 1

    if basename == "Desktop.in":
        # last arg was changed in registryd between 2.38.0 and 2.45.90
        # from "(so)" to a properties dict for consistency with the
        # spec; keep the old version for now
        signal_arg = elt.find('./interface[@name="org.a11y.atspi.Event.Object"]'
                              '/signal[@name="ChildrenChanged"]'
                              '/arg[@name="properties"]')
        assert(signal_arg.get("type") == "a{sv}")
        signal_arg.set("type", "(so)")

class _BuildMetaBackend(setuptools.build_meta._BuildMetaBackend):
    def build_sdist(self, sdist_directory, config_settings=None):
        # not sure it's the right way to find our source tree but hey
        source_dir = os.path.join(sdist_directory, "..")
        assert os.path.exists(os.path.join(source_dir, "at-spi2-core"))
        gen_xml_files(os.path.join(source_dir, "src/a14y/data"),
                      os.path.join(source_dir, "at-spi2-core/xml"))
        return super().build_sdist(sdist_directory, config_settings)

_BACKEND = _BuildMetaBackend()
get_requires_for_build_wheel = _BACKEND.get_requires_for_build_wheel
get_requires_for_build_sdist = _BACKEND.get_requires_for_build_sdist
prepare_metadata_for_build_wheel = _BACKEND.prepare_metadata_for_build_wheel
build_wheel = _BACKEND.build_wheel
build_sdist = _BACKEND.build_sdist
