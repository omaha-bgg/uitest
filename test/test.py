#!/usr/bin/env python3

import argparse
import asyncio
import logging
import os

from a14y import a14y

# locate test objects
import sys
testdir = os.path.dirname(sys.argv[0])

logger = logging.getLogger("main")

# handles for cleanup on exception
test_task = None
proc = None

async def testoneprog(atspi, python, testprog):
    global proc
    script = os.path.join(testdir, testprog)
    env = os.environ
    env.update(LC_ALL='C', LANGUAGE='',
               QT_LINUX_ACCESSIBILITY_ALWAYS_ON='1',
               GTK_MODULES='gail:atk-bridge')

    desktop = await atspi.get_desktop()
    await desktop.get_children() # request updates, FIXME

    logger.info("launching %s %r", python, script)
    proc = await asyncio.create_subprocess_exec(
        python, script, env=env, stdout=None, stderr=None)

    # get app

    app = await desktop.notify_app_add(testprog)
    logger.info("got app %s", app)
    tk = await app.get_toolkit()
    logger.info("toolkit %s version %s", tk.name, tk.version)
    states = await app.get_states()
    logger.info("states: %s", states)
    attributes = await app.get_attributes()
    logger.info("attributes: %s", attributes)

    # navigate to buttons

    toplevels = await app.get_children()
    logger.info("toplevels: %s", toplevels)
    assert len(toplevels) == 1
    toplevel = toplevels[0]
    logger.info("toplevel: %s", toplevel)

    fillers = await toplevel.get_children()
    assert len(fillers) == 1
    filler = fillers[0]
    logger.info("filler: %s", filler)

    buttons = await filler.get_children()
    logger.info("buttons: %s", [str(b) for b in buttons])

    # press "Open", expect dialog

    openbuttons = [b for b in buttons if await b.get_name() == "Open"]
    assert len(openbuttons) == 1
    openbutton = openbuttons[0]
    await tk.button_click(openbutton)

    # wait for dialog
    while len(await app.get_children(force=True)) < 2:
        await asyncio.sleep(0.1)
    dlg = (await app.get_children())[1]

    # press "Close", expect cleanup

    dlg_children = await dlg.get_children()
    logger.info("dialog children: %s", [str(b) for b in buttons])
    if len(dlg_children) == 1:
        # gtk has a "vbox" filler
        dlg_filler = dlg_children[0]
        assert await dlg_filler.get_role() == a14y.Accessible.Role.FILLER
        dlg_buttons = await dlg_filler.get_children()
    else:
        assert await dlg_children[0].get_role() == a14y.Accessible.Role.PUSH_BUTTON
        dlg_buttons = dlg_children
    closebuttons = [b for b in dlg_buttons if await b.get_name() == "Close"]
    assert len(closebuttons) == 1
    closebutton = closebuttons[0]
    await tk.button_click(closebutton)

    # wait for dialog closing
    while len(await app.get_children(force=True)) > 1:
        await asyncio.sleep(0.1)

    # press "Quit", expect cleanup

    quitbuttons = [b for b in buttons if await b.get_name() == "Quit"]
    assert len(quitbuttons) == 1
    quitbutton = quitbuttons[0]
    await tk.button_click(quitbutton)

    await desktop.notify_app_remove(app)
    logger.info("app %s was closed", app)

async def main():
    atspi = a14y.Atspi()

    for python, testprog in (
            ('python3', 'testobject-gtk3.py'),
            ('python3', 'testobject-pyqt5.py'),
            ('python3', 'testobject-pyside2.py'),
            ('python3', 'testobject-pyside6.py'),
    ):
        await testoneprog(atspi, python, testprog)

def cli_parser():
    parser = argparse.ArgumentParser(description="A14y self-test")
    parser.add_argument('-v', '--verbose', action="count", default=0,
                        help='increase verbosity level')
    return parser

def handle_exception(loop, context):
    loop.default_exception_handler(context)
    # context["message"] will always be there; but context["exception"] may not
    msg = context.get("exception", context["message"])
    logger.error(f"Caught exception: {msg}")
    logger.info("Shutting down...")
    if proc.returncode is None:
        proc.kill()
    else:
        logger.info("proc already terminated, status %d", proc.returncode)

    test_task.cancel()

if __name__ == '__main__':
    args = cli_parser().parse_args()
    if args.verbose == 0:
        LOGLEVEL = logging.WARNING
    elif args.verbose == 1:
        LOGLEVEL = logging.INFO
    else:
        LOGLEVEL = logging.DEBUG

    logging.basicConfig(
        level=LOGLEVEL,
        format='{asctime}|{levelname}|{name}: {message}', style='{')
    logging.getLogger("asyncio").setLevel(LOGLEVEL)

    asyncio.get_event_loop().set_debug(True)
    asyncio.get_event_loop().set_exception_handler(handle_exception)

    test_task = asyncio.get_event_loop().create_task(main())
    asyncio.get_event_loop().run_until_complete(test_task)
