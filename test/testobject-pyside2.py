#!/usr/bin/env python3

import PySide2 as PyQt
from PySide2 import QtCore, QtGui, QtWidgets
QAction = QtWidgets.QAction

import sys

app = QtWidgets.QApplication([sys.argv[0]])

window = QtWidgets.QMainWindow()
window.setWindowTitle("test app")

vbox = QtWidgets.QWidget(window)
vboxlayout = QtWidgets.QVBoxLayout(vbox)
window.setCentralWidget(vbox)

def button_action(w):
    dlg = QtWidgets.QDialog(window)
    dlg.setWindowTitle("test dialog")
    dlg.setAttribute(QtCore.Qt.WA_DeleteOnClose)

    layout = QtWidgets.QVBoxLayout(dlg)
    button = QtWidgets.QPushButton(vbox)
    button.setText("Close")
    def close_dlg(w):
        dlg.hide()
        dlg.destroy()
    button.clicked.connect(close_dlg)
    layout.addWidget(button)

    butbox = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok)
    layout.addWidget(butbox)
    dlg.show()

button = QtWidgets.QPushButton(vbox)
button.setText("Open")
button.clicked.connect(button_action)
vboxlayout.addWidget(button)

button = QtWidgets.QPushButton(vbox)
button.setText("Quit")
button.clicked.connect(app.closeAllWindows)
vboxlayout.addWidget(button)

window.show()

sys.exit(app.exec_())
