#!/usr/bin/env python3

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

window = Gtk.Window()
window.connect("destroy", lambda w: Gtk.main_quit())
window.set_title("test app")
window.set_default_size(200, 200)

vbox = Gtk.VBox()
window.add(vbox)

def button_action(w):
    dlg = Gtk.Dialog("test dialog")
    dlg.add_buttons(Gtk.STOCK_OK, Gtk.ResponseType.OK)
    button = Gtk.Button(label="Close")
    def close_dlg(w):
        dlg.hide()
        dlg.destroy()
    button.connect("clicked", close_dlg)
    dlg.vbox.pack_start(button, expand=False, fill=True, padding=0)
    dlg.show_all()

button = Gtk.Button(label="Open")
button.connect("clicked", button_action)
vbox.add(button)

button = Gtk.Button(label="Quit")
button.connect("clicked", lambda w: Gtk.main_quit())
vbox.add(button)

window.show_all()
Gtk.main()
