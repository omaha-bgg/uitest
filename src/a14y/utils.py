import asyncio
import logging

def setup_logging(args):
    if args.verbose == 0:
        log_level = logging.WARNING
    elif args.verbose == 1:
        log_level = logging.INFO
    else:
        log_level = logging.DEBUG

    logging.basicConfig(
        level=log_level,
        format='{asctime}|{levelname}|{name}: {message}', style='{')
    logging.getLogger("asyncio").setLevel(log_level)

    asyncio.get_event_loop().set_debug(args.verbose > 0)
