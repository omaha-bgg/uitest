#!/usr/bin/python3 -Xdev -Wdefault

# FIXME:
# - nodes not removed
# - Qt5, no ChildrenChanged signals ?  but holes in dbus serial#...

import asyncio
from enum import Enum
import logging

from importlib_resources import files as resource_files

import dbus_next
from dbus_next.aio import MessageBus

__all__ = (
    "atspibus get_desktop"
).split(' ')

logger = logging.getLogger("a14y")

INTROSPECTION = {}
resources = resource_files('a14y.data')
for key in "Registry Desktop Application Accessible".split(' '):
    xmldata = resources.joinpath(f'{key}.xml').read_text()
    INTROSPECTION[key] = dbus_next.introspection.Node.parse(xmldata)

async def open_atspibus():
    "Locate the bus for the main AT-SPI traffic"
    sessionbus = await MessageBus().connect()

    # hardcoded in at-spi2-core/bus/at-spi-bus-launcher.c
    a11y_bus_introspection = (
        "<node>"
        "  <interface name='org.a11y.Bus'>"
        "    <method name='GetAddress'>"
        "      <arg type='s' name='address' direction='out'/>"
        "    </method>"
        "  </interface>"
        "  <interface name='org.a11y.Status'>"
        "    <property name='IsEnabled' type='b' access='readwrite'/>"
        "    <property name='ScreenReaderEnabled' type='b' access='readwrite'/>"
        "  </interface>"
        "</node>"
    )

    a11y_bus = sessionbus.get_proxy_object('org.a11y.Bus', '/org/a11y/bus',
                                           a11y_bus_introspection)

    a11y_status_iface = a11y_bus.get_interface('org.a11y.Status')
    await a11y_status_iface.set_is_enabled(False)
    is_enabled = await a11y_status_iface.get_is_enabled()
    if not is_enabled:
        logger.info("enabling a11y first")
        await a11y_status_iface.set_is_enabled(True)
        is_enabled = await a11y_status_iface.get_is_enabled()
        assert is_enabled

    a11y_bus_iface = a11y_bus.get_interface('org.a11y.Bus')
    a11y_busname = await a11y_bus_iface.call_get_address()
    return await MessageBus(a11y_busname).connect()

class Accessible:
    Child = None # would set to Accessible if it was already defined at this point
    def __init__(self, bus, address, parent, *, introspectkey='Accessible', load_depth=0):
        assert isinstance(address, list) and len(address) == 2
        self.bus = bus
        self.address = address
        self.parent = parent
        self.app = parent.app if parent else None
        self.role, self.name = None, None
        self.obj = bus.get_proxy_object(self.address[0], self.address[1],
                                        INTROSPECTION[introspectkey])
        self.accessible_iface = self.obj.get_interface('org.a11y.atspi.Accessible')
        self.event_iface = self.obj.get_interface('org.a11y.atspi.Event.Object')
        self.children = None
        self.states = None
        self.attributes = None
        self.__load_depth = load_depth

        # Action is not supported by all Accessible's, filled later as needed
        self.action_iface = None
        self.actions = None

    async def async_init(self):
        await self.get_role()
        await self.get_name()
        if self.__load_depth:
            await self.get_children(load_depth=self.__load_depth - 1)
        return self

    def __await__(self):
        return self.async_init().__await__()

    Role = Enum('Accessible.Role', """ACCELERATOR_LABEL ALERT
        ANIMATION ARROW CALENDAR CANVAS CHECK_BOX CHECK_MENU_ITEM
        COLOR_CHOOSER COLUMN_HEADER COMBO_BOX DATE_EDITOR DESKTOP_ICON
        DESKTOP_FRAME DIAL DIALOG DIRECTORY_PANE DRAWING_AREA
        FILE_CHOOSER FILLER FOCUS_TRAVERSABLE FONT_CHOOSER FRAME
        GLASS_PANE HTML_CONTAINER ICON IMAGE INTERNAL_FRAME LABEL
        LAYERED_PANE LIST LIST_ITEM MENU MENU_BAR MENU_ITEM
        OPTION_PANE PAGE_TAB PAGE_TAB_LIST PANEL PASSWORD_TEXT
        POPUP_MENU PROGRESS_BAR PUSH_BUTTON RADIO_BUTTON
        RADIO_MENU_ITEM ROOT_PANE ROW_HEADER SCROLL_BAR SCROLL_PANE
        SEPARATOR SLIDER SPIN_BUTTON SPLIT_PANE STATUS_BAR TABLE
        TABLE_CELL TABLE_COLUMN_HEADER TABLE_ROW_HEADER
        TEAROFF_MENU_ITEM TERMINAL TEXT TOGGLE_BUTTON TOOL_BAR
        TOOL_TIP TREE TREE_TABLE UNKNOWN VIEWPORT WINDOW EXTENDED
        HEADER FOOTER PARAGRAPH RULER APPLICATION AUTOCOMPLETE EDITBAR
        EMBEDDED ENTRY CHART CAPTION DOCUMENT_FRAME HEADING PAGE
        SECTION REDUNDANT_OBJECT FORM LINK INPUT_METHOD_WINDOW
        TABLE_ROW TREE_ITEM DOCUMENT_SPREADSHEET DOCUMENT_PRESENTATION
        DOCUMENT_TEXT DOCUMENT_WEB DOCUMENT_EMAIL COMMENT LIST_BOX
        GROUPING IMAGE_MAP NOTIFICATION INFO_BAR LEVEL_BAR TITLE_BAR
        BLOCK_QUOTE AUDIO VIDEO DEFINITION ARTICLE LANDMARK LOG
        MARQUEE MATH RATING TIMER STATIC MATH_FRACTION MATH_ROOT
        SUBSCRIPT SUPERSCRIPT DESCRIPTION_LIST DESCRIPTION_TERM
        DESCRIPTION_VALUE FOOTNOTE CONTENT_DELETION CONTENT_INSERTION
        MARK SUGGESTION PUSH_BUTTON_MENU""")

    State = Enum('Accessible.State',"""
        ACTIVE ARMED BUSY CHECKED COLLAPSED DEFUNCT EDITABLE ENABLED
        EXPANDABLE EXPANDED FOCUSABLE FOCUSED HAS_TOOLTIP HORIZONTAL
        ICONIFIED MODAL MULTI_LINE MULTISELECTABLE OPAQUE PRESSED
        RESIZABLE SELECTABLE SELECTED SENSITIVE SHOWING SINGLE_LINE
        STALE TRANSIENT VERTICAL VISIBLE MANAGES_DESCENDANTS
        INDETERMINATE REQUIRED TRUNCATED ANIMATED INVALID_ENTRY
        SUPPORTS_AUTOCOMPLETION SELECTABLE_TEXT IS_DEFAULT VISITED
        CHECKABLE HAS_POPUP READ_ONLY""")

    async def get_role(self, force=False):
        if force or self.role is None:
            self.role = Accessible.Role(await self.accessible_iface.call_get_role())
        return self.role

    async def get_name(self, force=False):
        if force or self.name is None:
            self.name = await self.accessible_iface.get_name()
        return self.name

    async def get_children(self, force=False, *, load_depth=0):
        """Returns list of children nodes.

        Caches the list and registers to get notified by
        children_changed so we don't have to make the AT-SPI call
        again.
        """
        logger.debug("get_children(%s, load_depth=%s)", self, load_depth)
        ChildClass = self.Child or Accessible
        if self.children is None:
            # setup handler to catch further updates
            self.event_iface.on_children_changed(self.handle_children_changed)
        if force or self.children is None:
            self.children = [ChildClass(self.bus, address, parent=self, load_depth=load_depth)
                             for address in await self.accessible_iface.call_get_children()]
            if self.children:
                await asyncio.wait(self.children)
            logger.debug("%s: children after get_: %s", self, [str(c) for c in self.children])
        return self.children

    async def handle_children_changed(self, operation, index_in_parent, _, variant, rest):
        assert variant.signature == '(so)'
        child_address = variant.value
        logger.debug("%s: children_changed %s #%s child=%s rest=%s",
                     self, operation, index_in_parent, child_address, rest)

        if operation == "add":
            ChildClass = self.Child or Accessible
            # FIXME check it is not here already (get_children race condition for message
            # emitted after we decide to request full children list but before it sends the
            # GetChildren reply) - can we fix it and still use dbus_next high-level API?
            while True: # never loops
                if index_in_parent != len(self.children):
                    logger.warning("inserting at index %s when len is %s",
                                   index_in_parent, len(self.children))
                    break
                self.children.insert(index_in_parent,
                                     ChildClass(self.bus, child_address, parent=self))
                await self.children[index_in_parent]
                logger.debug("%s: children after _changed: %s",
                             self, [str(c) for c in self.children])
                return

        if operation == "remove":
            if self.children[index_in_parent].address == child_address:
                self.children.pop(index_in_parent)
                logger.debug("%s: children after _changed: %s",
                             self, [str(c) for c in self.children])
                return
            logger.error("cannot remove %s at %s from %s",
                         child_address, index_in_parent, self.children)

        # fallback: this force-gets all children on every change.
        await self.get_children(force=True)

    async def get_states(self, force=False):
        # FIXME: caches but does not register for changes
        if force or self.states is None:
            self.states = []
            for fieldnum, field32 in enumerate(await self.accessible_iface.call_get_state()):
                for bit in range(32):
                    if field32 & 1:
                        self.states.append(Accessible.State(bit + 32 * fieldnum))
                    field32 = field32 >> 1
                assert field32 == 0, f"{field32:x} != 0"
        return self.states

    async def get_attributes(self, force=False):
        # FIXME maybe: caches but does not register for changes
        if force or self.attributes is None:
            self.attributes = await self.accessible_iface.call_get_attributes()
        return self.attributes

    def __ensure_actions_iface(self):
        if not self.action_iface:
            self.action_iface = self.obj.get_interface('org.a11y.atspi.Action')

    async def get_actions(self, force=False):
        self.__ensure_actions_iface()

        # FIXME: we still want to use it for toolkits supporting it
#        try:
#            # Gtk 3.24.24, 3.24.30 aborts on that call
#            return await self.action_iface.call_get_actions()
#        except dbus_next.errors.DBusError as ex:
#            logger.error("no GetActions?")

        if force or self.actions is None:
            try:
                n_actions = await self.action_iface.get_n_actions()
            except dbus_next.errors.DBusError:
                logger.error("no NActions?")
                return []

            self.actions = [await self.action_iface.call_get_name(i) for i in range(n_actions)]
        return self.actions

    async def do_action(self, action_name):
        logger.debug("%s: do_action(%r)", self, action_name)
        action_list = await self.get_actions()
        action_index = action_list.index(action_name)
        return await self.action_iface.call_do_action(action_index)

    def __str__(self):
        return f"<{type(self).__name__}({self.address!r}) role={self.role} name={self.name!r}>"

class Application(Accessible):
    def __init__(self, bus, address, parent, *, load_depth=0):
        assert address[1] == '/org/a11y/atspi/accessible/root'
        super().__init__(bus, address, parent,
                         introspectkey='Application', load_depth=load_depth)
        self.app = self
        self.application_iface = self.obj.get_interface('org.a11y.atspi.Application')
        self.toolkit = None

    async def async_init(self):
        await super().async_init()
        assert self.role == Accessible.Role.APPLICATION

    # FIXME: use async_init instead?
    async def get_toolkit(self):
        if self.toolkit:
            return self.toolkit

        toolkit_name = await self.application_iface.get_toolkit_name()
        toolkit_version = await self.application_iface.get_version()

        if toolkit_name == "gtk": # v3 (or more?)
            self.toolkit = GtkToolkit(toolkit_name, toolkit_version)
        #elif toolkit_name == "GAIL": # Gtk v2
        #    self.toolkit = GtkToolkit(toolkit_name, toolkit_version)
        elif toolkit_name == "Qt":
            self.toolkit = QtToolkit(toolkit_name, toolkit_version)
        if self.toolkit is None:
            raise RuntimeError("Unsupported toolkit")

        return self.toolkit

    def __str__(self):
        return f"<{type(self).__name__}({self.address[0]!r}) name={self.name!r}>"

class Registry:
    def __init__(self, bus):
        self.bus = bus
        self.obj = bus.get_proxy_object(
            'org.a11y.atspi.Registry', '/org/a11y/atspi/registry',
            INTROSPECTION['Registry'])
        self.registry_iface = self.obj.get_interface('org.a11y.atspi.Registry')
        self.registry_iface.on_event_listener_registered(self.handle_event_listener_registered)

    def handle_event_listener_registered(self, bus, path, properties):
        logger.debug("EventListenerRegistered(%s, %s, %s)", bus, path, properties)

class Desktop(Accessible):
    Child = Application
    def __init__(self, bus):
        super().__init__(bus, ['org.a11y.atspi.Registry', '/org/a11y/atspi/accessible/root'],
                         parent=None, introspectkey='Desktop')
        self.__app_add_futures = {}
        self.__app_remove_futures = {}

    async def async_init(self):
        await super().async_init()
        assert self.role == Accessible.Role.DESKTOP_FRAME

    async def handle_children_changed(self, operation, index_in_parent, _, variant, rest):
        logger.debug("Desktop.handle_children_changed(%s)", operation)
        await super().handle_children_changed(operation, index_in_parent, _, variant, rest)
        app_address = variant.value
        if operation == "add":
            app = self.children[index_in_parent]
            assert app.address == app_address
            if app.name in self.__app_add_futures:
                self.__app_add_futures[app.name].set_result(app)
                del self.__app_add_futures[app.name]
        if operation == "remove":
            hashable_address = tuple(app_address)
            if hashable_address in self.__app_remove_futures:
                self.__app_remove_futures[hashable_address].set_result(True)
                del self.__app_remove_futures[hashable_address]

    async def notify_app_add(self, appname):
        assert appname not in self.__app_add_futures
        self.__app_add_futures[appname] = asyncio.get_running_loop().create_future()
        return await self.__app_add_futures[appname]
    async def notify_app_remove(self, app):
        hashable_address = tuple(app.address)
        assert hashable_address not in self.__app_remove_futures
        self.__app_remove_futures[hashable_address] = asyncio.get_running_loop().create_future()
        return await self.__app_remove_futures[hashable_address]

    def __str__(self):
        return f"<{type(self).__name__}()>"

class Atspi:
    """A proxy object to the AT-SPI subsystem.
    """
    def __init__(self):
        self.bus = None
        self.registry = None
        self.desktop = None

    async def get_desktop(self):
        if self.desktop:
            return self.desktop

        self.bus = await open_atspibus()
        self.registry = Registry(self.bus)
        await self.registry.registry_iface.call_register_event("object")
        self.desktop = Desktop(self.bus)
        await self.desktop
        return self.desktop


class Toolkit:
    def __init__(self, name, version):
        self.name, self.version = name, version

class GtkToolkit(Toolkit):
    "Collection of Gtk AT-SPI idiosyncrasies"
    async def button_click(self, accessible):
        await accessible.do_action("click")

class QtToolkit(Toolkit):
    "Collection of Qt AT-SPI idiosyncrasies"
    async def button_click(self, accessible):
        await accessible.do_action("Press")
