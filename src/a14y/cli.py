import argparse
import asyncio
import logging
import sys

from . import a14y
from . import utils

test_task = None # initialized in main()

def cli_parser():
    parser = argparse.ArgumentParser(description="AT-SPI command-line client")
    parser.add_argument('-v', '--verbose', action="count", default=0,
                        help='increase verbosity level')
    parser.add_argument('-d', '--depth', type=int, default=0,
                        help='look for DEPTH levels of children')
    parser.add_argument('app', nargs='?',
                        help='D-BUS service of application to detail')
    return parser

def handle_exception(loop, context):
    loop.default_exception_handler(context)
    # context["message"] will always be there; but context["exception"] may not
    msg = context.get("exception", context["message"])
    logging.error("Caught exception: %s", msg)
    logging.info("Shutting down...")

    test_task.cancel()

async def print_children(accessible, indent, depth):
    spaces = " " * indent
    for child in await accessible.get_children():
        print(f"{spaces}{child}")
        if depth > 0:
            await print_children(child, indent + 1, depth - 1)

async def cli_main(args):
    atspi = a14y.Atspi()

    desktop = await atspi.get_desktop()
    if not args.app:
        # list all apps
        print(desktop)
        await print_children(desktop, indent=1, depth=args.depth)
        sys.exit(0)

    # list details for requested app
    apps = [app for app in await desktop.get_children()
            if app.address[0] == args.app]
    for app in apps:
        print(app)
        await print_children(app, indent=1, depth=args.depth)
    sys.exit(0)

def main():
    global test_task

    args = cli_parser().parse_args()
    utils.setup_logging(args)

    asyncio.get_event_loop().set_exception_handler(handle_exception)

    test_task = asyncio.get_event_loop().create_task(cli_main(args))
    asyncio.get_event_loop().run_until_complete(test_task)

if __name__ == '__main__':
    main()
