#! /usr/bin/env python3

import argparse
import asyncio
import logging

from dbus_next import Message, MessageType

from . import a14y
from . import utils

logger = logging.getLogger("test-lowlevel")

def print_message(msg):
    typ = {MessageType.METHOD_CALL: "CAL",
           MessageType.METHOD_RETURN: "RET",
           MessageType.SIGNAL: "SIG",
           MessageType.ERROR: "ERR"}[msg.message_type]
    text = (f"{msg.sender} {typ}#{msg.serial} {msg.path} {msg.interface}"
            f" {msg.member}({msg.signature})")
    if msg.destination:
        text += f"\n dst={msg.destination}"
    text += f"\n {msg.body}"
    print(text)

async def monitor(app=None):
    atspi = a14y.Atspi()
    await atspi.get_desktop()

    atspi.bus.add_message_handler(print_message)
    #match_rule = f"type='signal',sender={bus_name},interface={introspection.name},path={path}"
    if app:
        match_rule = f"type='signal',sender={app}"
    else:
        match_rule = "type='signal'"
    reply = await atspi.bus.call(
        Message(destination='org.freedesktop.DBus',
                interface='org.freedesktop.DBus',
                path='/org/freedesktop/DBus',
                member='AddMatch',
                signature='s',
                body=[match_rule]))
    assert reply.message_type != MessageType.ERROR

    await atspi.bus.wait_for_disconnect()


def cli_parser():
    parser = argparse.ArgumentParser(description="AT-SPI event monitor")
    parser.add_argument('-v', '--verbose', action="count", default=0,
                        help='increase verbosity level')
    parser.add_argument('app', nargs='?',
                        help='D-BUS service of application to monitor (default: all apps)')
    return parser

def run_monitor():
    args = cli_parser().parse_args()
    utils.setup_logging(args)

    #asyncio.get_event_loop().set_exception_handler(handle_exception)

    mon_task = asyncio.get_event_loop().create_task(monitor(args.app))
    asyncio.get_event_loop().run_until_complete(mon_task)

if __name__ == '__main__':
    run_monitor()
